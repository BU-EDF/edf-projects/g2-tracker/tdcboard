source vhdl/sim/i2c_helper.tcl
# source vhdl/sim/i2c_generate.tcl

isim force add /i2c_slave/address 1010101 -radix bin
isim force add /i2c_slave/reset 0
isim force add /i2c_slave/data_in 01010101 -radix bin

isim force add /i2c_slave/SDA 1
isim force add /i2c_slave/SCL 1

run 1000 us

send_start /i2c_slave/sda /i2c_slave/scl
send_byte 10101010 /i2c_slave/sda /i2c_slave/scl
send_byte 00000000 /i2c_slave/sda /i2c_slave/scl
send_byte 00000001 /i2c_slave/sda /i2c_slave/scl
send_byte 00000010 /i2c_slave/sda /i2c_slave/scl
send_byte 00000011 /i2c_slave/sda /i2c_slave/scl
send_stop /i2c_slave/sda /i2c_slave/scl

run 1000 us

send_start /i2c_slave/sda /i2c_slave/scl
send_byte 10101010 /i2c_slave/sda /i2c_slave/scl
send_byte 00000100 /i2c_slave/sda /i2c_slave/scl
send_byte 00000001 /i2c_slave/sda /i2c_slave/scl
send_byte 00000010 /i2c_slave/sda /i2c_slave/scl
send_byte 00000011 /i2c_slave/sda /i2c_slave/scl
send_stop /i2c_slave/sda /i2c_slave/scl

run 1000 us

send_start /i2c_slave/sda /i2c_slave/scl
send_byte 10101010 /i2c_slave/sda /i2c_slave/scl
send_byte 00000100 /i2c_slave/sda /i2c_slave/scl
send_restart /i2c_slave/sda /i2c_slave/scl
send_byte 10101011 /i2c_slave/sda /i2c_slave/scl
get_byte /i2c_slave/sda /i2c_slave/scl
get_byte /i2c_slave/sda /i2c_slave/scl
get_byte /i2c_slave/sda /i2c_slave/scl
send_stop /i2c_slave/sda /i2c_slave/scl


run 1000 us

