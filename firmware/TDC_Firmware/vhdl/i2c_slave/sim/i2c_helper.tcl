#generate one i2c send sequence
proc send_start { i2c_SDA i2c_SCL } {
    isim force add $i2c_SDA 0
    run 10 us
    isim force add $i2c_SCL 0
}

proc send_restart { i2c_SDA i2c_SCL } {
    isim force add $i2c_SDA 1
    run 2.5 us
    isim force add $i2c_SCL 1
    run 2.5 us
    isim force add $i2c_SDA 0
    run 2.5 us
    isim force add $i2c_SCL 0
    run 2.5us
}

proc send_byte { i2c_data i2c_SDA i2c_SCL } {
    #send byte
    set n [ string length $i2c_data ]
    for { set i 0 } { $i < $n } { incr i } {
	set bit [string index $i2c_data $i ]
	run 2.5 us
	if { $bit == "1" } {
	    isim force add $i2c_SDA 1
	} else {
	    isim force add $i2c_SDA 0
	}
	run 2.5 us
	isim force add $i2c_SCL 1
	run 5 us
	isim force add $i2c_SCL 0
    }

    run 2.5 us
    isim force remove $i2c_SDA
    run 2.5 us
    isim force add $i2c_SCL 1
    run 5 us
    isim force add $i2c_SCL 0
    run 7.5 us
}

proc get_byte { i2c_SDA i2c_SCL } {
    #read 
#    isim force remove $i2c_SDA
    for { set i 0 } { $i < 8 } { incr i } {
	run 5 us
	isim force add $i2c_SCL 1
	run 5 us
	isim force add $i2c_SCL 0
    }
    run 2.5 us
    isim force add $i2c_SDA 0
    run 2.5 us
    isim force add $i2c_SCL 1
    run 5 us
    isim force add $i2c_SCL 0
    run 2.5 us
    isim force remove $i2c_SDA 
    run 7.5 us
}


proc send_stop { i2c_SDA i2c_SCL } {
    run 2.5 us
    
    run 5 us
    isim force add $i2c_SCL 1
    run 10 us
    isim force add $i2c_SDA 1
}




#proc send_data_cycle { i2c_addr i2c_data stop i2c_SDA i2c_SCL } {
#    isim force add $i2c_SDA 0
#    run 10 us
#
#    #send address
#    set n [ string length $i2c_addr ]
#    for { set i 0 } { $i < $n } { incr i } {
#	set bit [string index $i2c_addr $i ]
#	isim force add $i2c_SCL 0
#	run 2.5 us
#	if { $bit == "1" } {
#	    isim force add $i2c_SDA 1
#	} else {
#	    isim force add $i2c_SDA 0
#	}
#	run 2.5 us
#	isim force add $i2c_SCL 1
#	run 5 us
#    }
#    
#    #write bit
#    isim force add $i2c_SCL 0
#    run 2.5us
#    isim force add $i2c_SDA 0
#    run 2.5us
#    isim force add $i2c_SCL 1
#    run 5us
#    isim force add $i2c_SCL 0
#    run 2.5us
#    #ack bit
#    isim force remove $i2c_SDA
#    run 2.5 us
#    isim force add $i2c_SCL 1
#    run 5 us
#    isim force add $i2c_SCL 0
#    run 7.5 us
#
#    #incoming data
#    set n [string length $i2c_data]
#    for { set i 0 } { $i < $n } { incr i } {
#	set bit [string index $i2c_data $i ]
#	isim force add $i2c_SCL 0
#	run 2.5 us
#	if { $bit == "1" } {
#	    isim force add $i2c_SDA 1
#	} else {
#	    isim force add $i2c_SDA 0
#	}
#	run 2.5 us
#	isim force add $i2c_SCL 1
#	run 5 us
#    }
#    #ack bit
#    isim force add $i2c_SCL 0
#    run 2.5 us
#    isim force remove $i2c_SDA
#    run 2.5 us
#    isim force add $i2c_SCL 1
#    run 5 us
#    isim force add $i2c_SCL 0
#    run 2.5 us
#
#    if { $stop == "0" } {
#	run 2.5 us
#
#	isim force add $i2c_SDA 1
#	run 5 us
#	isim force add $i2c_SCL 1
#	run 10 us
#    } else {
#	run 2.5 us
#
#	run 5 us
#	isim force add $i2c_SCL 1
#	run 10 us
#	isim force add $i2c_SDA 1
#    }
#}


