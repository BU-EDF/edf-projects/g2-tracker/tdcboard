library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity DACController is
  
  port (
    clk     : in  std_logic;
    wr      : in  std_logic;            -- Begin write statement
    channel : in  std_logic_vector(1 downto 0);           -- Channel select
    value   : in  std_logic_vector(11 downto 0); 
    sync    : out std_logic;            -- DAC sync
    data    : out std_logic;            -- DAC data
    sclk    : out std_logic);           -- DAC sclk

end DACController;          
architecture Behavioral of DACController is
  constant counter_start      : std_logic_vector (5 downto 0) := "100011";  -- initial counter value (states -1)
  constant counter_sync_end   : std_logic_vector (5 downto 0) := "011111"; -- end of sync window
  constant counter_data_start : std_logic_vector (5 downto 0) := "100000"; -- end of sync window

  
  signal counter : std_logic_vector (5 downto 0) := "111111";  -- State machine counter
  
  signal data_stream : std_logic_vector(15 downto 0) := X"3000";  -- power down
                                                                  -- all outputs

  signal wr_pulse : std_logic := '0';   -- turn wr into a pulse

begin  -- Behavioral

  DACWriteSetup: process(clk)
    begin
      if clk'event and clk = '1' then
        wr_pulse <= wr;                 -- store last state of wr
        
        -------------------------------------------------------------------------------
        -- Setup write sequence counter
        -------------------------------------------------------------------------------
        if counter = "111111" and wr = '1' and wr_pulse = '0' then
          -- load state machien counter
          counter <= counter_start;
        elsif counter /= "111111" then
          counter <= std_logic_vector(unsigned(counter) - 1);
        else
          --nothing          
		  end if;
      end if;
    end process DACWriteSetup;

  DACWriteCommand: process(clk)
    begin
      if clk'event and clk = '1' then

        -- write nothign to the DAC by default
        sclk <= '0';
        sync <= '0';
        data <= '0';

        -------------------------------------------------------------------------------
        -- Setup write sequence data
        -------------------------------------------------------------------------------
        if counter = "111111" and wr = '1' and wr_pulse = '0' then
          -- setup which channel we are writing to
          data_stream(15 downto 14) <= channel;
          --set write command to write to one channel and enable output
          data_stream(13 downto 12) <= "01";
          -- load data          if counter < counter_start-3 then
          data_stream(11 downto 0) <= value;
        -------------------------------------------------------------------------------
        -- Stream the serial data to DAC
        -------------------------------------------------------------------------------
        elsif counter /= "111111" then      
          -- process serial clock signal
          sclk <= std_logic_vector(counter)(0);      
              
          -- process sync signal
          if counter < counter_start and counter > counter_sync_end then
            -- sequence starts with a 1.5 clock length pulse on sync
            -- delayed by 0.5 clock ticks
            sync <= '1';
          else
            sync <= '0';        
          end if;

          -- process data signal
          if counter < counter_data_start then
            -- clock out the MSB of data_stream to the data line
            data <= data_stream(15);
        
            -- if counter(0) is 0, then we need to change the data bit on the next
            -- clock transition, so bitshift data_stream 1 to the left.
            if std_logic_vector(counter)(0) = '0' then
              data_stream(15 downto 0) <= data_stream(14 downto 0) & data_stream(15);
            end if;          
          else
            -- data should be silent here
            data <= '0';          
          end if;
        end if;
      end if;
    end process DACWriteCommand;
  end Behavioral;
  

--
--DACUpdate: process(clk)
--begin
--  if clk'event and clk = '1' then
--
--    -------------------------------------------------------------------------------
--    -- Setup write sequence
--    -------------------------------------------------------------------------------
--    if counter = "111111" and wr = '1' then
--      -- load state machien counter
--      counter <= counter_start;
--      -- setup which channel we are writing to
--      data_stream(15 downto 14) <= channel;
--      --set write command to write to one channel and enable output
--      data_stream(13 downto 12) <= "01";
--      -- load data
--      data_stream(11 downto 0) <= value;
--
--      
--    -------------------------------------------------------------------------------
--    -- Do nothing
--    -------------------------------------------------------------------------------        
--    elsif counter = "111111" and wr = '0' then        
--      -- write nothign to the DAC
--      sclk <= '0';
--      sync <= '0';
--      data <= '0';
--
--
--      
--    -------------------------------------------------------------------------------
--    -- Stream the serial data to DAC
--    -------------------------------------------------------------------------------
--    elsif counter >= "00000" then      
--      -- step back in the counter for next clk
--      counter <= counter - "000001";
--
--      -- process serial clock signal
--      sclk <= std_logic_vector(counter)(0);      
--            
--      -- process sync signal
--      if counter < counter_start and counter > (counter_start - 4) then
--        -- sequence starts with a 1.5 clock length pulse on sync
--        -- delayed by 0.5 clock ticks
--        sync <= '1';
--      else
--        sync <= '0';        
--      end if;
--
--      -- process data signal
--      if counter < counter_start-3 then
--        -- clock out the MSB of data_stream to the data line
--        data <= data_stream(15);
--      
--        -- if counter(0) is 0, then we need to change the data bit on the next
--        -- clock transition, so bitshift data_stream 1 to the left.
--        if std_logic_vector(counter)(0) = '0' then
--          data_stream(15 downto 0) <= data_stream(14 downto 0) & data_stream(15);
--        end if;          
--      else
--        -- data should be silent here
--        data <= '0';          
--      end if;
--    end if;
--
--    
--  end if;       
--end process DACUpdate;
--end Behavioral;
